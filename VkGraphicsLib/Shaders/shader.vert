#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec2 pos;
layout(location = 1) in vec3 color;

layout(location = 0) out vec3 fragColor;

layout(set = 0, binding = 0) uniform MatrixBuff {
	mat4 modelToWorld;
	mat4 projection;
	mat4 view;
} matBuffObj;

void main() 
{
	gl_Position = matBuffObj.projection * matBuffObj.view * matBuffObj.modelToWorld * vec4(pos, 0.0, 1.0);
	fragColor = color;
}