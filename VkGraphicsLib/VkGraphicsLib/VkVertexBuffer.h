#pragma once
#include "stdafx.h"

struct VertexPosColorAttributeDesc
{
	VkVertexInputAttributeDescription inputAttribDesc[2];
};

struct Vertex
{
	static VkVertexInputBindingDescription getBindDesc()
	{
		VkVertexInputBindingDescription bindDesc = {};
		bindDesc.binding = 0;
		bindDesc.stride = sizeof(Vertex);
		bindDesc.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;
		return bindDesc;
	}

	static VertexPosColorAttributeDesc getPosColorAttributeDescriptions()
	{
		VertexPosColorAttributeDesc vertAttribDesc = {};

		// Position
		vertAttribDesc.inputAttribDesc[0].binding = 0;
		vertAttribDesc.inputAttribDesc[0].location = 0;
		vertAttribDesc.inputAttribDesc[0].format = VK_FORMAT_R32G32_SFLOAT;
		vertAttribDesc.inputAttribDesc[0].offset = offsetof(Vertex, pos);

		// Color
		vertAttribDesc.inputAttribDesc[1].binding = 0;
		vertAttribDesc.inputAttribDesc[1].location = 1;
		vertAttribDesc.inputAttribDesc[1].format = VK_FORMAT_R32G32B32_SFLOAT;
		vertAttribDesc.inputAttribDesc[1].offset = offsetof(Vertex, col);

		return vertAttribDesc;
	}

	Vector2 pos;
	Color col;
};

class VkGraphics;

class VkVertexBuffer {
public:
	VkVertexBuffer();
	~VkVertexBuffer();

	void Initialize(VkGraphics* graphics);
	void UploadData(void* vertData, int numVertices, size_t vertStride);
	void SetActive(VkCommandBuffer& commandBuff);
	void Release();

private:
	VkGraphics* mGraphics;
	VkDevice *mDevice;
	VkBuffer mVertexBuffer;
	VkDeviceMemory mVertexBufferMemory;
	VkBuffer mIndexBuffer;
	VkDeviceMemory mIndexBufferMemory;
};

