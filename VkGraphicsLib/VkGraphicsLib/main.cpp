#include "stdafx.h"
#include "VkGraphics.h"

int main(int argc, const char * argv[]) 
{
    VkGraphics g;
    
    g.InitVulkan();
    g.Run();
    g.CleanUp();
    return 0; 
}
