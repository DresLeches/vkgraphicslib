# VkGraphicsLib

A Vulkan Graphics Library to use for the Toon Engine and any kind of graphics work. I picked Vulkan
since it is cross-platform. As of now, this library only supports MacOS. Windows support will be 
added in the near future.
