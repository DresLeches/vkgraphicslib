#pragma once
#include <cstdio>
#define GRAPHICS_DEBUG 1

// Set up validation layer for normal Vulkan
const std::vector<const char*> validationLayers = {
	"VK_LAYER_KHRONOS_validation",
	"VK_LAYER_LUNARG_assistant_layer"
};

#if GRAPHICS_DEBUG
const bool enableValidationLayers = true;
#else
const bool enableValidationLayers = false;
#endif

// TODO: Look into adding more debug callback messages
static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(
	VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
	VkDebugUtilsMessageTypeFlagsEXT messageType,
	const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
	void* pUserData)
{
	printf("validation layer: %s\n", pCallbackData->pMessage);
	return VK_FALSE;
}

struct VkDebugFunc {
    static void InterpretVkResult(VkResult result);
};

struct VkDebugWindow {
    VkResult CreateDebugUtilsMessengerEXT(VkInstance instance,
                                          const VkDebugUtilsMessengerCreateInfoEXT* pCreateInfo,
                                          const VkAllocationCallbacks* pAllocator,
                                          VkDebugUtilsMessengerEXT* pCallback);
    void DestroyDebugUtilsMessengerEXT(VkInstance instance, VkDebugUtilsMessengerEXT callback,
                                       const VkAllocationCallbacks* pAllocator);
};
