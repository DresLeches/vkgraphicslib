#include "stdafx.h"
#include "VkDebug.h"

/****************************************************************************************/
/*                                                                                      */
/*  VkDebugFunc: Debug Functions                                                        */
/*                                                                                      */
/****************************************************************************************/

// VkResult Info Source: https://vulkan.lunarg.com/doc/view/1.0.30.0/linux/vkspec.chunked/ch02s06.html
void VkDebugFunc::InterpretVkResult(VkResult result)
{
    switch(result)
    {
        case VK_NOT_READY:
            perror("VK_NOT_READY: A fence or query was not completed\n");
            break;
        case VK_TIMEOUT:
            perror("VK_TIMEOUT: A wait operation failed to complete in specified time\n");
            break;
        case VK_EVENT_SET:
            perror("VK_EVENT_SET: A event is signaled\n");
            break;
        case VK_EVENT_RESET:
            perror("VK_EVENT_RESET: An event is unsignaled\n");
            break;
        case VK_INCOMPLETE:
            perror("VK_INCOMPLETE: A return array was too small for the result\n");
            break;
        case VK_SUBOPTIMAL_KHR:
            perror("VK_SUBOPTIMAL_KHR: A swapchain no longer matches the surface properties exactly, but can be used to present to the surface successfully\n");
            break;
        case VK_ERROR_OUT_OF_HOST_MEMORY:
            perror("VK_ERROR_OUT_OF_HOST_MEMORY: A host memory allocation has failed\n");
            break;
        case VK_ERROR_OUT_OF_DEVICE_MEMORY:
            perror("VK_ERROR_OUT_OF_DEVICE_MEMORY: A device memory allocation has failed\n");
            break;
        case VK_ERROR_INITIALIZATION_FAILED:
            perror("VK_ERROR_INITIALIZATION_FAILED: Initialization of an object could not be completed for implementation-specific reasons\n");
            break;
        case VK_ERROR_DEVICE_LOST:
            perror("VK_ERROR_DEVICE_LOST: A logical or physical device has been lost\n");
            break;
        case VK_ERROR_MEMORY_MAP_FAILED:
			perror("VK_ERROR_MEMORY_MAP_FAILED: Mapping of a memory object has failed\n");
            break;
        case VK_ERROR_LAYER_NOT_PRESENT:
			perror("VK_ERROR_LAYER_NOT_PRESENT: A requested layer is not present or coudl not be loaded\n");
            break;
        case VK_ERROR_EXTENSION_NOT_PRESENT:
			perror("VK_ERROR_EXTENSION_NOT_PRESENT: A requested extension is not supported\n");
            break;
        case VK_ERROR_FEATURE_NOT_PRESENT:
			perror("VK_ERROR_FEATURE_NOT_PRESENT: A requested feature is not supported\n");
            break;
        case VK_ERROR_INCOMPATIBLE_DRIVER:
			perror("VK_ERROR_INCOMPATIBLE_DRIVER: The requested version of Vulkan is not supported by the driver or is otherwise incompatible for implementation-specific reasons\n");
            break;
        case VK_ERROR_TOO_MANY_OBJECTS:
			perror("VK_ERROR_TOO_MANY_OBJECTS: Too many objects of the type have already been created\n");
            break;
        case VK_ERROR_FORMAT_NOT_SUPPORTED:
			perror("VK_ERROR_FORMAT_NOT_SUPPORTED: A requested format is not supported on this device\n");
            break;
        case VK_ERROR_FRAGMENTED_POOL:
			perror("VK_ERROR_FRAGMENTED_POOL: A requested pool allocation has failed due to fragmentation of the pool’s memory\n");
            break;
        case VK_ERROR_SURFACE_LOST_KHR:
			perror("VK_ERROR_SURFACE_LOST_KHR: A surface is no longer available\n");
            break;
        case VK_ERROR_NATIVE_WINDOW_IN_USE_KHR:
			perror("VK_ERROR_NATIVE_WINDOW_IN_USE_KHR: The requested window is already connected to a VkSurfaceKHR, or to some other non-Vulkan API\n");
            break;
        case VK_ERROR_OUT_OF_DATE_KHR:
			perror("VK_ERROR_OUT_OF_DATE_KHR: A surface has changed in such a way that it is no longer compatible with the swapchain, and further presentation requests using the swapchain will fail. Applications must query the new surface properties and recreate their swapchain if they wish to continue presenting to the surface\n");
            break;
        case VK_ERROR_INCOMPATIBLE_DISPLAY_KHR:
			perror("VK_ERROR_INCOMPATIBLE_DISPLAY_KHR: The display used by a swapchain does not use the same presentable image layout, or is incompatible in a way that prevents sharing an image\n");
            break;
        default:
			perror("Invalid VKResult\n");
    }
}


/****************************************************************************************/
/*                                                                                      */
/*  VkDebugWindow: Debug Window Functions                                               */
/*                                                                                      */
/****************************************************************************************/
VkResult VkDebugWindow::CreateDebugUtilsMessengerEXT(VkInstance instance,
                                                     const VkDebugUtilsMessengerCreateInfoEXT* pCreateInfo,
                                                     const VkAllocationCallbacks* pAllocator,
                                                     VkDebugUtilsMessengerEXT* pCallback)
{
    auto func = (PFN_vkCreateDebugUtilsMessengerEXT) vkGetInstanceProcAddr(instance,
                                                                           "vkCreateDebugUtilsMessengerEXT");
    if (func != nullptr) 
	{
        return func(instance, pCreateInfo, pAllocator, pCallback);
    } 
	else 
	{
        return VK_ERROR_EXTENSION_NOT_PRESENT;
    }
}

void VkDebugWindow::DestroyDebugUtilsMessengerEXT(VkInstance instance, VkDebugUtilsMessengerEXT callback,
                                                  const VkAllocationCallbacks* pAllocator)
{
    auto func = (PFN_vkDestroyDebugUtilsMessengerEXT) vkGetInstanceProcAddr(instance, "vkDestroyDebugUtilsMessengerEXT");
    if (func != nullptr) 
	{
        func(instance, callback, pAllocator);
    }
}
