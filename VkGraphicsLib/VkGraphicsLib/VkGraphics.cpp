#include "stdafx.h"
#include "VkGraphics.h"
#include "VkVertexBuffer.h"

const int MAX_FRAMES_IN_USE = 2;

static void framebufferResizeCallback(GLFWwindow* window, int width, int height);

static std::vector<Vertex> vertices = {
	{
		Vector2(-0.5f, -0.5f),
		Color(1.0f, 0.0f, 0.0f)
	},
	{
		Vector2(0.5f, -0.5f),
		Color(1.0f, 1.0f, 1.0f)
	},
	{
		Vector2(0.5f, 0.5f),
		Color(0.0f, 1.0f, 0.0f)
	},
	{
		Vector2(-0.5f, 0.5f),
		Color(0.0f, 0.0f, 1.0f)
	}
};

static std::vector<uint16_t> indices = {
	0, 1, 2, 2, 3, 0
};



class DeviceScoreComparator {
public:
    bool operator() (const VkPhysicalDeviceScore& lhs, const VkPhysicalDeviceScore& rhs)
    {
        return lhs.score < rhs.score;
    }
};

VkGraphics::VkGraphics()
	:mCurrentFrame(0)
	,mFrameBufferResized(false)
{
}

VkGraphics::~VkGraphics()
{
}

void VkGraphics::InitVulkan() 
{

    // Initialize the GLFW
    glfwInit();
    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
    glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);   // This will be for now
    mWindow = glfwCreateWindow(_WIDTH_, _HEIGHT_, "Vulkan", nullptr, nullptr);
	glfwSetWindowUserPointer(mWindow, this);
	glfwSetFramebufferSizeCallback(mWindow, framebufferResizeCallback);

	// Build the windows
    createInstance();
	setUpDebugCallback();
	createVkSurface();
    createPhysicalDevice();
    initLogicalDevice();
    initSwapChain();
	createRenderPass();
	createDescriptorSetLayout();
	createGraphicsPipeline();
	createFrameBuffers();
	createCommandPool();
	initBuffers();
	createDescriptorPool();
	createDescriptorSets();
	createCommandBuffers();
	createSemaphore();
}

void VkGraphics::Run() 
{
    while (!glfwWindowShouldClose(mWindow)) 
	{
        glfwPollEvents();
		drawFrame();
    }

	// Wait for all operations to finish
	vkDeviceWaitIdle(mVkDevice);
}

void VkGraphics::CleanUp() 
{
    if (enableValidationLayers) 
	{
        mVkDebugWindow.DestroyDebugUtilsMessengerEXT(mInstance, callback, nullptr);
    }

	cleanupSwapChain();
	vkDestroyDescriptorSetLayout(mVkDevice, mVkDescSetLayout, nullptr);

	for (int i = 0; i < MAX_FRAMES_IN_USE; i++)
	{
		vkDestroySemaphore(mVkDevice, mVkRenderFinishedSemaphores[i], nullptr);
		vkDestroySemaphore(mVkDevice, mVkSwapchainImageSemaphores[i], nullptr);
		vkDestroyFence(mVkDevice, mVkSwapchainImageFences[i], nullptr);
	}

	vkDestroyCommandPool(mVkDevice, mVkCommandPool, nullptr);
	vkDestroySurfaceKHR(mInstance, mVkSurface, nullptr);
	vkDestroyDevice(mVkDevice, nullptr);
    vkDestroyInstance(mInstance, nullptr);
    glfwDestroyWindow(mWindow);
    glfwTerminate();
}

bool VkGraphics::CreateVertexBuffer(VkBuffer& vertBuff, VkDeviceMemory& deviceMem)
{
	// Create the stage buffer
	VkBuffer stagingBuffer;
	VkDeviceMemory stagingMemory;
	VkDeviceSize bufferSize = vertices.size() * sizeof(Vertex);
	CreateBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, 
					VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, stagingBuffer, stagingMemory);
	
	void* data = nullptr;
	vkMapMemory(mVkDevice, stagingMemory, 0, bufferSize, 0, &data);
	memcpy(data, vertices.data(), (size_t)bufferSize);
	vkUnmapMemory(mVkDevice, stagingMemory);
	
	// Create the actual vertex buffer
	VkDeviceSize vertSize = vertices.size() * sizeof(Vertex);
	CreateBuffer(vertSize, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, 
								VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, vertBuff, deviceMem);

	CopyBuffer(&vertBuff, &stagingBuffer, bufferSize);
	vkDestroyBuffer(mVkDevice, stagingBuffer, nullptr);
	vkFreeMemory(mVkDevice, stagingMemory, nullptr);
	return true;
}

bool VkGraphics::CreateIndexBuffer(VkBuffer& indBuff, VkDeviceMemory& deviceMem)
{
	VkDeviceSize indSize = indices.size() * sizeof(uint16_t);
	VkBuffer stagingBuffer;
	VkDeviceMemory stagingMemory;
	CreateBuffer(indSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
					VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, stagingBuffer, stagingMemory);

	void* data = nullptr;
	vkMapMemory(mVkDevice, stagingMemory, 0, indSize, 0, &data);
	memcpy(data, indices.data(), (size_t)indSize);
	vkUnmapMemory(mVkDevice, stagingMemory);

	CreateBuffer(indSize, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT,
					VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, indBuff, deviceMem);

	CopyBuffer(&indBuff, &stagingBuffer, indSize);
	vkDestroyBuffer(mVkDevice, stagingBuffer, nullptr);
	vkFreeMemory(mVkDevice, stagingMemory, nullptr);
	return true;
}

/****************************************************************************************/
/*                                                                                      */
/*  Graphics: Private functions                                                         */
/*                                                                                      */
/****************************************************************************************/
void VkGraphics::createInstance() 
{

    if (enableValidationLayers && !checkValidationLayerSupport()) 
	{
        printf("Valdiation Layer could not be found\n");
    }
 
    // Tell Vulkan what we need for our application
    VkApplicationInfo appInfo = {};
    appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
    appInfo.pApplicationName = "Hello Triangle";
#if __APPLE__ && __MACH__
    appInfo.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
#elif _WIN32 || _WIN64
	appInfo.applicationVersion = VK_MAKE_VERSION(1, 1, 92);
#endif
	appInfo.pEngineName = "Vulkan Debug";
    appInfo.engineVersion = VK_MAKE_VERSION(1, 0, 0);
    appInfo.apiVersion = VK_API_VERSION_1_0;

    // Tell Vulkan driver which global extensions and validation layers we want.
    // Global means that they apply to the entire program and not a specific
    // device
    VkInstanceCreateInfo createInfo = {};
    createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
    createInfo.pApplicationInfo = &appInfo;
    if (enableValidationLayers) 
	{
        createInfo.enabledLayerCount = static_cast<uint32_t> (mSupportedValidationLayers.size());
        createInfo.ppEnabledLayerNames = mSupportedValidationLayers.data();
    }
    else 
	{
        createInfo.enabledLayerCount = 0;
    }

    // Vulkan is agnostic, so you need an extension to interface with the window system
	enumerateVkExtensions();
    getRequiredGLFWExtensions(mDesiredExtensions);

	// MoltenVK doesn't support debug windows
	if (!_MOLTEN_VK_ && enableValidationLayers)
	{
		mDesiredExtensions.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
	}

	// Enable the extensions we want
    createInfo.enabledExtensionCount = (uint32_t) mDesiredExtensions.size();
    createInfo.ppEnabledExtensionNames = mDesiredExtensions.data();
    createInfo.enabledLayerCount = 0;

    // We have everything we need to create a simple instance! Time to call it
    VkResult result = vkCreateInstance(&createInfo, nullptr, &mInstance);
    if (result != VK_SUCCESS) 
	{
        printf("Failed to create instance!\n");
    }
}

static void framebufferResizeCallback(GLFWwindow* window, int width, int height)
{
	auto app = reinterpret_cast<VkGraphics*>(glfwGetWindowUserPointer(window));
	app->ToggleBufferFrameResized(true);
}

bool VkGraphics::checkExtensionSupport(std::vector<VkExtensionProperties> &extensions,
                                 const char** glfwExtensions, uint32_t extCount)
{
    bool supported = true;
    for (auto props : extensions)
	{
        for (uint32_t i = 0; i < extCount; i++) 
		{
			if (strcmp(props.extensionName, glfwExtensions[i]) == 0)
			{
				supported = true;
				break;
			}
        }
    }
    return supported;
}



// FIXME: Figure out why vulkan loader can't find the other validation layer on MacOS
bool VkGraphics::checkValidationLayerSupport() 
{
    uint32_t layerCount;
    vkEnumerateInstanceLayerProperties(&layerCount, nullptr);

    std::vector<VkLayerProperties> layers(layerCount);
    vkEnumerateInstanceLayerProperties(&layerCount, layers.data());

    for (const char* layerName : validationLayers) 
	{
        for (const auto& layerProp : layers) 
		{
            if (strcmp(layerName, layerProp.layerName) == 0) 
			{
                mSupportedValidationLayers.push_back(layerProp.layerName);
                return true;
            }
        }
    }
    
    return false;
}

void VkGraphics::enumerateVkExtensions()
{
	// Get an array of supported extension details
	uint32_t vkExtensionCount = 0;
	vkEnumerateInstanceExtensionProperties(nullptr, &vkExtensionCount, nullptr);
	mVkExtensions.resize(vkExtensionCount);
	vkEnumerateInstanceExtensionProperties(nullptr, &vkExtensionCount, mVkExtensions.data());
}


void VkGraphics::getRequiredGLFWExtensions(std::vector<const char*>& ext)
{
    uint32_t glfwExtensionCount = 0;
    const char** glfwExtensions;
    glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtensionCount);

    // Check if the extension is supported
    if (!checkExtensionSupport(mVkExtensions, glfwExtensions, glfwExtensionCount)) 
	{
        printf("GLFW Extension is not supported\n");
    }

    // Return the supported extension after everything is finished
	for (int i = 0; i < glfwExtensionCount; i++)
	{
		ext.push_back(glfwExtensions[i]);
	}
}

void VkGraphics::setUpDebugCallback() 
{
    if (!enableValidationLayers) 
	{
        return;
    }
    VkDebugUtilsMessengerCreateInfoEXT createInfo = {};
    createInfo.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
    createInfo.messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT |
                                 VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT |
							     VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
    createInfo.messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT |
					         VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT |
						     VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
	createInfo.pfnUserCallback = debugCallback;
    createInfo.pUserData = nullptr; 

    VkResult result = mVkDebugWindow.CreateDebugUtilsMessengerEXT(mInstance, &createInfo, nullptr, &callback);
    if (result != VK_SUCCESS)
    {
        VkDebugFunc::InterpretVkResult(result);
        printf("Could not create the Debug Messenger\n"); 
    }
}

bool VkGraphics::createDescriptorSetLayout()
{
	// Set up the descriptor layout binding
	VkDescriptorSetLayoutBinding matLayoutBinding = {};
	matLayoutBinding.binding = 0;
	matLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	matLayoutBinding.descriptorCount = 1;
	matLayoutBinding.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
	matLayoutBinding.pImmutableSamplers = nullptr;

	// Set up the descriptor create info
	VkDescriptorSetLayoutCreateInfo layoutCreateInfo = {};
	layoutCreateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
	layoutCreateInfo.bindingCount = 1;
	layoutCreateInfo.pBindings = &matLayoutBinding;

	VkResult res = vkCreateDescriptorSetLayout(mVkDevice, &layoutCreateInfo, nullptr, &mVkDescSetLayout);
	if (res != VK_SUCCESS)
	{
		VkDebugFunc::InterpretVkResult(res);
		return false;
	}
	return true;
}

bool VkGraphics::createGraphicsPipeline()
{
	bool res = true;
	std::vector<char> vertShader;
	std::vector<char> fragShader;

	// Read the file
	res = readShaderFile("shaders/vert.spv", vertShader);
	if (res == false)
	{
		printf("Failed to read the vertex shader file\n");
		return false;
	}

	res = readShaderFile("shaders/frag.spv", fragShader);
	if (res == false)
	{
		printf("Failed to read the fragment shader file\n");
		return false;
	}

	// Create the shader module
	res = createShaderModule(vertShader, &mVkVertexShaderModule);
	if (res == false)
	{
		printf("Failed to create the vertex shader module\n");
		return false;
	}

	res = createShaderModule(fragShader, &mVkFragShaderModule);
	if (res == false)
	{
		printf("Failed to create the fragment shader module\n");
		return false;
	}

	// Assign the shader module to their respective shader
	VkPipelineShaderStageCreateInfo vertexShaderStageCreateInfo = {};
	vertexShaderStageCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
	vertexShaderStageCreateInfo.stage = VK_SHADER_STAGE_VERTEX_BIT;
	vertexShaderStageCreateInfo.module = mVkVertexShaderModule;
	vertexShaderStageCreateInfo.pName = "main";

	VkPipelineShaderStageCreateInfo fragShaderStageCreateInfo = {};
	fragShaderStageCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
	fragShaderStageCreateInfo.stage = VK_SHADER_STAGE_FRAGMENT_BIT;
	fragShaderStageCreateInfo.module = mVkFragShaderModule;
	fragShaderStageCreateInfo.pName = "main";

	VkPipelineShaderStageCreateInfo shaderStageCreateInfo[] = {
		vertexShaderStageCreateInfo,
		fragShaderStageCreateInfo
	};

	// Create the vertex input pipeline
	VkVertexInputBindingDescription inputBindDesc = Vertex::getBindDesc();
	VertexPosColorAttributeDesc vertPosColAttrib = Vertex::getPosColorAttributeDescriptions();


	VkPipelineVertexInputStateCreateInfo vertexInputStateCreateInfo = {};
	vertexInputStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
	vertexInputStateCreateInfo.vertexBindingDescriptionCount = 1;
	vertexInputStateCreateInfo.vertexAttributeDescriptionCount = (uint32_t) (sizeof(vertPosColAttrib) / sizeof(VkVertexInputAttributeDescription));
	vertexInputStateCreateInfo.pVertexBindingDescriptions = &inputBindDesc;
	vertexInputStateCreateInfo.pVertexAttributeDescriptions = &vertPosColAttrib.inputAttribDesc[0];

	// Create the input assember pipeline
	VkPipelineInputAssemblyStateCreateInfo inputAssemblyStateCreateInfo = {};
	inputAssemblyStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
	inputAssemblyStateCreateInfo.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
	inputAssemblyStateCreateInfo.primitiveRestartEnable = VK_FALSE;

	// Create the viewport
	VkViewport vp = {};
	vp.x = 0.0f;
	vp.y = 0.0f;
	vp.width = (float) mVkSwapChainExtent.width;
	vp.height = (float) mVkSwapChainExtent.height;
	vp.minDepth = 0.0f;
	vp.maxDepth = 1.0f;

	VkRect2D scissor = {};
	scissor.extent = mVkSwapChainExtent;
	scissor.offset = { 0, 0 };

	VkPipelineViewportStateCreateInfo viewportStateCreateInfo = {};
	viewportStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
	viewportStateCreateInfo.viewportCount = 1;
	viewportStateCreateInfo.pViewports = &vp;
	viewportStateCreateInfo.scissorCount = 1;
	viewportStateCreateInfo.pScissors = &scissor;

	// Create the rasterization pipeline
	VkPipelineRasterizationStateCreateInfo rastaStateCreateInfo = {};
	rastaStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
	rastaStateCreateInfo.depthClampEnable = VK_FALSE;
	rastaStateCreateInfo.rasterizerDiscardEnable = VK_FALSE;
	rastaStateCreateInfo.polygonMode = VK_POLYGON_MODE_FILL;
	rastaStateCreateInfo.cullMode = VK_CULL_MODE_BACK_BIT;
	rastaStateCreateInfo.frontFace = VK_FRONT_FACE_CLOCKWISE;
	rastaStateCreateInfo.depthBiasEnable = VK_FALSE;
	rastaStateCreateInfo.depthBiasConstantFactor = 0.0f;
	rastaStateCreateInfo.depthBiasClamp = 0.0f;
	rastaStateCreateInfo.depthBiasSlopeFactor = 0.0f;
	rastaStateCreateInfo.lineWidth = 1.0f;

	// Create stage for multisampling. OPTIONAL
	// TODO: Refactor and put this into its own function
	VkPipelineMultisampleStateCreateInfo multisampleStateCreateInfo = {};
	multisampleStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
	multisampleStateCreateInfo.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
	multisampleStateCreateInfo.sampleShadingEnable = VK_FALSE;
	multisampleStateCreateInfo.minSampleShading = 1.0f;
	multisampleStateCreateInfo.pSampleMask = nullptr;
	multisampleStateCreateInfo.alphaToCoverageEnable = VK_FALSE;
	multisampleStateCreateInfo.alphaToOneEnable = VK_FALSE;

	// Create the blending stage
	VkPipelineColorBlendAttachmentState colorBlendAttachmentState = {};
	colorBlendAttachmentState.blendEnable = VK_FALSE;
	colorBlendAttachmentState.srcColorBlendFactor = VK_BLEND_FACTOR_ONE;
	colorBlendAttachmentState.dstColorBlendFactor = VK_BLEND_FACTOR_ZERO;
	colorBlendAttachmentState.colorBlendOp = VK_BLEND_OP_ADD;
	colorBlendAttachmentState.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
	colorBlendAttachmentState.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
	colorBlendAttachmentState.colorWriteMask = VK_COLOR_COMPONENT_R_BIT |
											   VK_COLOR_COMPONENT_G_BIT |
											   VK_COLOR_COMPONENT_B_BIT |
											   VK_COLOR_COMPONENT_A_BIT;

	VkPipelineColorBlendStateCreateInfo colorBlendStateCreateInfo = {};
	colorBlendStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
	colorBlendStateCreateInfo.logicOpEnable = VK_FALSE;
	colorBlendStateCreateInfo.logicOp = VK_LOGIC_OP_COPY;
	colorBlendStateCreateInfo.attachmentCount = 1;
	colorBlendStateCreateInfo.pAttachments = &colorBlendAttachmentState;
	colorBlendStateCreateInfo.blendConstants[0] = 0.0f;
	colorBlendStateCreateInfo.blendConstants[1] = 0.0f;
	colorBlendStateCreateInfo.blendConstants[2] = 0.0f;
	colorBlendStateCreateInfo.blendConstants[3] = 0.0f;

	// Create the dynamic state
	VkDynamicState dynamicStates[] = {
		VK_DYNAMIC_STATE_VIEWPORT,
		VK_DYNAMIC_STATE_LINE_WIDTH
	};

	VkPipelineDynamicStateCreateInfo dynamicStateCreateInfo = {};
	dynamicStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
	dynamicStateCreateInfo.dynamicStateCount = 2;
	dynamicStateCreateInfo.pDynamicStates = dynamicStates;

	// Create the pipeline stage for the layout
	VkPipelineLayoutCreateInfo layoutCreateInfo = {};
	layoutCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
	layoutCreateInfo.setLayoutCount = 1;
	layoutCreateInfo.pSetLayouts = &mVkDescSetLayout;
	layoutCreateInfo.pushConstantRangeCount = 0;
	layoutCreateInfo.pPushConstantRanges = nullptr;
	

	VkResult vkRes = vkCreatePipelineLayout(mVkDevice, &layoutCreateInfo, nullptr, &mVkPipelineLayout);
	if (vkRes != VK_SUCCESS)
	{
		VkDebugFunc::InterpretVkResult(vkRes);
		printf("Failed to create the pipeline layout\n");
		return false;
	}

	VkGraphicsPipelineCreateInfo pipelineCreateInfo = {};
	pipelineCreateInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
	pipelineCreateInfo.stageCount = 2;
	pipelineCreateInfo.pStages = shaderStageCreateInfo;
	pipelineCreateInfo.pVertexInputState = &vertexInputStateCreateInfo;
	pipelineCreateInfo.pInputAssemblyState = &inputAssemblyStateCreateInfo;
	pipelineCreateInfo.pTessellationState = nullptr;
	pipelineCreateInfo.pViewportState = &viewportStateCreateInfo;
	pipelineCreateInfo.pRasterizationState = &rastaStateCreateInfo;
	pipelineCreateInfo.pMultisampleState = &multisampleStateCreateInfo;
	pipelineCreateInfo.pDepthStencilState = nullptr;
	pipelineCreateInfo.pColorBlendState = &colorBlendStateCreateInfo;
	pipelineCreateInfo.layout = mVkPipelineLayout;
	pipelineCreateInfo.renderPass = mVkRenderPass;
	pipelineCreateInfo.subpass = 0;
	pipelineCreateInfo.basePipelineHandle = VK_NULL_HANDLE;
	pipelineCreateInfo.basePipelineIndex = -1;

	vkRes = vkCreateGraphicsPipelines(mVkDevice, VK_NULL_HANDLE, 1, &pipelineCreateInfo, 
									  nullptr, &mVkPipeline);
	if (vkRes != VK_SUCCESS)
	{
		VkDebugFunc::InterpretVkResult(vkRes);
		printf("Failed to create the graphics pipeline\n");
		return false;
	}

	vkDestroyShaderModule(mVkDevice, mVkVertexShaderModule, nullptr);
	vkDestroyShaderModule(mVkDevice, mVkFragShaderModule, nullptr);

	return true;
}

bool VkGraphics::createRenderPass()
{
	VkAttachmentDescription colorAttachDesc = {};
	colorAttachDesc.format = mVkSwapChainImageFormat;
	colorAttachDesc.samples = VK_SAMPLE_COUNT_1_BIT;
	colorAttachDesc.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
	colorAttachDesc.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
	colorAttachDesc.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
	colorAttachDesc.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
	colorAttachDesc.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	colorAttachDesc.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

	VkAttachmentReference colorAttachRef = {};
	colorAttachRef.attachment = 0;
	colorAttachRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

	VkSubpassDescription subpass = {};
	subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
	subpass.colorAttachmentCount = 1;
	subpass.pColorAttachments = &colorAttachRef;

	VkRenderPassCreateInfo renderPassCreateInfo = {};
	renderPassCreateInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
	renderPassCreateInfo.attachmentCount = 1;
	renderPassCreateInfo.pAttachments = &colorAttachDesc;
	renderPassCreateInfo.subpassCount = 1;
	renderPassCreateInfo.pSubpasses = &subpass;

	VkSubpassDependency vkSubpassDep = {};
	vkSubpassDep.srcSubpass = VK_SUBPASS_EXTERNAL;
	vkSubpassDep.dstSubpass = 0;
	vkSubpassDep.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	vkSubpassDep.srcAccessMask = 0;
	vkSubpassDep.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	vkSubpassDep.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
	renderPassCreateInfo.dependencyCount = 1;
	renderPassCreateInfo.pDependencies = &vkSubpassDep;

	VkResult res = vkCreateRenderPass(mVkDevice, &renderPassCreateInfo, nullptr, &mVkRenderPass);
	if (res != VK_SUCCESS)
	{
		VkDebugFunc::InterpretVkResult(res);
		printf("Failed to create the render pass\n");
		return false;
	}

	return true;
}

bool VkGraphics::createVkSurface()
{
    VkResult result;  // Used for debugging
    
#if _WIN32 || _WIN64
    
    result = glfwCreateWindowSurface(mInstance, mWindow, nullptr, &mVkSurface);
	if (result != VK_SUCCESS)
	{
        VkDebugFunc::InterpretVkResult(result);
		printf("Failed to create the VkSurface\n");
		return false;
	}
#elif __APPLE__ && __MACH__
    
    result = glfwCreateWindowSurface(mInstance, mWindow, nullptr, &mVkSurface);
    if (result != VK_SUCCESS)
    {
        VkDebugFunc::InterpretVkResult(result);
        printf("Failed to create the VkSurface\n");
        return false;
    }
#endif

	return true;
}

bool VkGraphics::createPhysicalDevice()
{
    // Select the extensions we want
    std::vector<const char*> tempExtensionList = {
        VK_KHR_SWAPCHAIN_EXTENSION_NAME
    };
    mDesiredDeviceExtensions = tempExtensionList;

    // Enumerate the physical device
    uint32_t pDeviceCount = 0;
    vkEnumeratePhysicalDevices(mInstance, &pDeviceCount, nullptr);

    // check the device support
    if (pDeviceCount == 0)
    {
        printf("Failed to find GPUs with Vulkan Support\n");
        return false;
    }

    // Pick the physical device
    getPhysicalDevice(pDeviceCount, &mVkPhysicalDevice);
    if (mVkPhysicalDevice == VK_NULL_HANDLE)
    {
        printf("Failed to find a suitable GPU Device\n");
        return false;
    }
    
    return true;
}

bool VkGraphics::initLogicalDevice()
{
    // For now, only interested in the queue with graphics capabilities
    VkDeviceQueueCreateInfo queueInfo = {};
    queueInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
    queueInfo.queueFamilyIndex = mIndex.graphicsFamily;
    queueInfo.queueCount = 1;

    // Schedule the command buffer execution on multiple threads based on priority
    float queuePriority = 1.0f;
    queueInfo.pQueuePriorities = &queuePriority;   

    // Leave it empty for now
	VkPhysicalDeviceFeatures deviceFeatures = {};
    
    VkDeviceCreateInfo createInfo = {};
    createInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
    createInfo.pQueueCreateInfos = &queueInfo;
    createInfo.queueCreateInfoCount = 1;
    createInfo.pEnabledFeatures = &deviceFeatures;
	createInfo.ppEnabledExtensionNames = mDesiredDeviceExtensions.data();
    createInfo.enabledExtensionCount = mDesiredDeviceExtensions.size();

    if (enableValidationLayers) 
	{
        createInfo.enabledLayerCount = static_cast<uint32_t>(validationLayers.size());
        createInfo.ppEnabledLayerNames = validationLayers.data();
    }
    else 
	{
        createInfo.enabledLayerCount = 0;
    }

    // Get the unique queue families
    std::vector<VkDeviceQueueCreateInfo> queueCreateInfos;
    std::set<uint32_t> uniqueQueueFamilies = { mIndex.graphicsFamily, mIndex.presentFamily };
    for (uint32_t queueFamily : uniqueQueueFamilies)
    {
        VkDeviceQueueCreateInfo queueCreateInfo = { };
        queueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
        queueCreateInfo.queueFamilyIndex = queueFamily;
        queueCreateInfo.queueCount = 1;
        queueCreateInfo.pQueuePriorities = &queuePriority;
        queueCreateInfos.push_back(queueCreateInfo);
    }
    createInfo.queueCreateInfoCount = static_cast<uint32_t>(queueCreateInfos.size());
    createInfo.pQueueCreateInfos = queueCreateInfos.data();
 
    VkResult result = vkCreateDevice(mVkPhysicalDevice, &createInfo, nullptr, &mVkDevice);
    if (result != VK_SUCCESS)
	{
        VkDebugFunc::InterpretVkResult(result);
        return false;
    }

	vkGetDeviceQueue(mVkDevice, mIndex.presentFamily, 0, &mPresentQueue);
    return true;
}

void VkGraphics::initBuffers()
{
	mVertexBuffer.Initialize(this); // Initialize the vertex buffer
	createUniformBuffers();

}

void VkGraphics::createUniformBuffers()
{
	VkDeviceSize buffSize = sizeof(VkMatrixBuff);
	mVkUniformBuffer.resize(mVkSwapchainImages.size());
	mVkUniformBufferMemory.resize(mVkSwapchainImages.size());

	for (size_t i = 0; i < mVkSwapchainImages.size(); i++)
	{
		CreateBuffer(buffSize, VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
			VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
			mVkUniformBuffer[i], mVkUniformBufferMemory[i]);
	}
}

bool VkGraphics::createDescriptorPool()
{
	VkDescriptorPoolSize descPoolSize = {};
	descPoolSize.type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	descPoolSize.descriptorCount = (uint32_t) mVkSwapchainImages.size();

	VkDescriptorPoolCreateInfo descPoolCreateInfo = {};
	descPoolCreateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
	descPoolCreateInfo.poolSizeCount = 1;
	descPoolCreateInfo.pPoolSizes = &descPoolSize;
	descPoolCreateInfo.maxSets = (uint32_t) mVkSwapchainImages.size();

	VkResult res = vkCreateDescriptorPool(mVkDevice, &descPoolCreateInfo, nullptr, &mVkDescPool);
	if (res != VK_SUCCESS)
	{
		VkDebugFunc::InterpretVkResult(res);
		return false;
	}

	return true;
}

bool VkGraphics::createDescriptorSets()
{
	std::vector<VkDescriptorSetLayout> descSetLayout(mVkSwapchainImages.size(), mVkDescSetLayout);
	VkDescriptorSetAllocateInfo descSetAllocInfo = {};
	descSetAllocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
	descSetAllocInfo.descriptorPool = mVkDescPool;
	descSetAllocInfo.descriptorSetCount = (uint32_t) mVkSwapchainImages.size();
	descSetAllocInfo.pSetLayouts = descSetLayout.data();

	mVkDescSet.resize(mVkSwapchainImages.size());
	VkResult res = vkAllocateDescriptorSets(mVkDevice, &descSetAllocInfo, mVkDescSet.data());
	if (res != VK_SUCCESS)
	{
		VkDebugFunc::InterpretVkResult(res);
		return false;
	}

	// Populate the the descriptor set
	for (size_t i = 0; i < mVkSwapchainImages.size(); i++)
	{
		VkDescriptorBufferInfo descBuffInfo = {};
		descBuffInfo.buffer = mVkUniformBuffer[i];
		descBuffInfo.offset = 0;
		descBuffInfo.range = sizeof(VkMatrixBuff);

		VkWriteDescriptorSet writeDescSet = {};
		writeDescSet.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		writeDescSet.dstSet = mVkDescSet[i];
		writeDescSet.dstBinding = 0;
		writeDescSet.dstArrayElement = 0;
		writeDescSet.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
		writeDescSet.descriptorCount = 1;
		writeDescSet.pBufferInfo = &descBuffInfo;
		writeDescSet.pImageInfo = nullptr;
		writeDescSet.pTexelBufferView = nullptr;

		vkUpdateDescriptorSets(mVkDevice, 1, &writeDescSet, 0, nullptr);
	}

	return true;
}

bool VkGraphics::initSwapChain()
{
	// Set the swapchain to default values
	mOldVkSwapchain = VK_NULL_HANDLE;
	mVkSwapchain = VK_NULL_HANDLE;

	// Initialize swapchain setting info
	VkSwapChainInfo swapChainInfo = {};
	VkSurfaceFormatKHR desiredSurfaceFormat = { VK_FORMAT_R8G8B8A8_UNORM, VK_COLOR_SPACE_SRGB_NONLINEAR_KHR };
	VkPresentModeKHR desiredPresentMode = VK_PRESENT_MODE_MAILBOX_KHR;
	VkImageUsageFlags desiredImageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
	VkSurfaceTransformFlagBitsKHR desiredTransform = VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR;

	if (querySwapChainSupport(mVkPhysicalDevice, &swapChainInfo) == false)
    {
        return false;
    }
	getSupportedImageDimension(swapChainInfo.capabilities, &swapChainInfo.imageSize);
	getSurfaceFormat(swapChainInfo.surfaceFormatList, desiredSurfaceFormat, &swapChainInfo.surfaceFormat);
	getPresentMode(swapChainInfo.presentModeList, desiredPresentMode, &swapChainInfo.presentMode);
	getNumSwapChainImages(swapChainInfo.capabilities, &swapChainInfo.numImages);
	getImageUsage(swapChainInfo.capabilities, desiredImageUsage, &swapChainInfo.imageUsage);
	getSurfaceTransformFlag(swapChainInfo.capabilities, desiredTransform, &swapChainInfo.transformFlag);
	selectSwapChainImageFormat(swapChainInfo.surfaceFormatList, desiredSurfaceFormat, &swapChainInfo.imageFormat, &swapChainInfo.imageColorSpace);

	// Initialize swapchain
	VkResult res = VK_SUCCESS;

	VkSwapchainCreateInfoKHR swapChainCreateInfo = {};
	swapChainCreateInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
	swapChainCreateInfo.surface = mVkSurface;
	swapChainCreateInfo.minImageCount = swapChainInfo.numImages;
	swapChainCreateInfo.imageFormat = swapChainInfo.imageFormat;
	swapChainCreateInfo.imageColorSpace = swapChainInfo.imageColorSpace;
	swapChainCreateInfo.imageExtent = swapChainInfo.imageSize;
	swapChainCreateInfo.imageArrayLayers = 1;
	swapChainCreateInfo.imageUsage = swapChainInfo.imageUsage;
	swapChainCreateInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
	swapChainCreateInfo.preTransform = swapChainInfo.capabilities.currentTransform;
	swapChainCreateInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
	swapChainCreateInfo.presentMode = swapChainInfo.presentMode;
	swapChainCreateInfo.clipped = VK_TRUE;
	
	res = vkCreateSwapchainKHR(mVkDevice, &swapChainCreateInfo, nullptr, &mVkSwapchain);
	if (res != VK_SUCCESS || mVkSwapchain == VK_NULL_HANDLE)
	{
		return false;
	}
	vkDestroySwapchainKHR(mVkDevice, mOldVkSwapchain, nullptr);
	mOldVkSwapchain = VK_NULL_HANDLE;

	// Set member variable formats and extent
	mVkSwapChainImageFormat = swapChainInfo.imageFormat;
	mVkSwapChainExtent = swapChainInfo.imageSize;

    // Get the swapchain images
	if (getSwapchainImageList(mVkSwapchainImages) == false)
	{
		printf("Could not get the list of swapchain images\n");
		return false;
	}

	// Create the image views
	if (createImageViews() == false)
	{
		printf("Failed to create the image views\n");
		return false;
	}

	return true;
}

void VkGraphics::recreateSwapChain()
{
	int width = 0;
	int height = 0;

	// Store the size of the frame buffer before minization and wait
	while (width == 0 || height == 0)
	{
		glfwGetFramebufferSize(mWindow, &width, &height);
		glfwWaitEvents();
	}

	vkDeviceWaitIdle(mVkDevice);

	initSwapChain();
	createImageViews();
	createRenderPass();
	createGraphicsPipeline();
	createFrameBuffers();
	createUniformBuffers();
	createDescriptorPool();
	createDescriptorSets();
	createCommandBuffers();
}

void VkGraphics::cleanupSwapChain()
{
	for (size_t i = 0; i < mVkFrameBuffer.size(); i++)
	{
		vkDestroyFramebuffer(mVkDevice, mVkFrameBuffer[i], nullptr);
	}

	vkFreeCommandBuffers(mVkDevice, mVkCommandPool, mVkCommandBuffers.size(), mVkCommandBuffers.data());
	vkDestroyPipeline(mVkDevice, mVkPipeline, nullptr);
	vkDestroyPipelineLayout(mVkDevice, mVkPipelineLayout, nullptr);
	vkDestroyRenderPass(mVkDevice, mVkRenderPass, nullptr);

	for (size_t i = 0; i < mVkSwapChainImageViews.size(); i++)
	{
		vkDestroyImageView(mVkDevice, mVkSwapChainImageViews[i], nullptr);
	}

	vkDestroySwapchainKHR(mVkDevice, mVkSwapchain, nullptr);

	// Destroy the uniform buffer 
	for (size_t i = 0; i < mVkSwapchainImages.size(); i++)
	{
		vkDestroyBuffer(mVkDevice, mVkUniformBuffer[i], nullptr);
		vkFreeMemory(mVkDevice, mVkUniformBufferMemory[i], nullptr);
	}

	// Destroy the descriptor pool
	vkDestroyDescriptorPool(mVkDevice, mVkDescPool, nullptr);
}

VkExtent2D VkGraphics::chooseSwapExtent(const VkSurfaceCapabilitiesKHR& capabilities)
{
#if _WIN32 || _WIN64
	uint32_t maxLen = UINT_MAX;
#elif __APPLE__ || __MACH__
	uint32_t maxLen = std::numeric_limits<uint32_t> max();
#endif

	if (capabilities.currentExtent.width != maxLen)
	{
		return capabilities.currentExtent;
	}
	else
	{
		int width = 0;
		int height = 0;
		glfwGetFramebufferSize(mWindow, &width, &height);

		VkExtent2D extent = {
			(uint32_t) width,
			(uint32_t) height
		};
		return extent;
	}
}

bool VkGraphics::readShaderFile(const std::string& fileName, std::vector<char>& outBuffer)
{
	std::ifstream shaderFile(fileName, std::ios::ate | std::ios::binary);

	if (!shaderFile.is_open())
	{
		printf("Failed to open the shader file\n");
		return false;
	}

	size_t fileSize = (size_t)shaderFile.tellg();
	std::vector<char> buffer(fileSize);
	shaderFile.seekg(0);
	shaderFile.read(buffer.data(), fileSize);
	shaderFile.close();
	outBuffer = buffer;
	return true;
}

bool VkGraphics::createShaderModule(const std::vector<char>& shader, VkShaderModule* outShaderModule)
{
	// Create the shader module create info
	VkShaderModuleCreateInfo shaderModuleCreateInfo = {};
	shaderModuleCreateInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
	shaderModuleCreateInfo.codeSize = shader.size();
	shaderModuleCreateInfo.pCode = reinterpret_cast<const uint32_t*>(shader.data());

	VkResult res = vkCreateShaderModule(mVkDevice, &shaderModuleCreateInfo, 
											nullptr, outShaderModule);
	if (res != VK_SUCCESS)
	{
		VkDebugFunc::InterpretVkResult(res);
		printf("Failed to create the shader module\n");
		return false;
	}
	return true;
}

bool VkGraphics::createFrameBuffers()
{
	mVkFrameBuffer.resize(mVkSwapChainImageViews.size());

	// Loop through image views and create framebuffers for them
	for (size_t i = 0; i < mVkSwapChainImageViews.size(); i++)
	{
		VkImageView attach[] = {
			mVkSwapChainImageViews[i]
		};

		VkFramebufferCreateInfo frameBufferCreateInfo = {};
		frameBufferCreateInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
		frameBufferCreateInfo.renderPass = mVkRenderPass;
		frameBufferCreateInfo.attachmentCount = 1;
		frameBufferCreateInfo.pAttachments = attach;
		frameBufferCreateInfo.width = mVkSwapChainExtent.width;
		frameBufferCreateInfo.height = mVkSwapChainExtent.height;
		frameBufferCreateInfo.layers = 1;

		VkResult res = vkCreateFramebuffer(mVkDevice, &frameBufferCreateInfo, nullptr, &mVkFrameBuffer[i]);
		if (res != VK_SUCCESS)
		{
			VkDebugFunc::InterpretVkResult(res);
			printf("Failed to create the frame buffer\n");
			return false;
		}
	}
	return true;
}

bool VkGraphics::createCommandPool()
{
	VkQueueFamilyIndex index = {};
	findQueueFamilies(mVkPhysicalDevice, &index);

	// Command pool create info
	VkCommandPoolCreateInfo commandPoolCreateInfo = {};
	commandPoolCreateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
	commandPoolCreateInfo.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
	commandPoolCreateInfo.queueFamilyIndex = index.graphicsFamily;

	VkResult result;
	result = vkCreateCommandPool(mVkDevice, &commandPoolCreateInfo, nullptr, &mVkCommandPool);
	if (result != VK_SUCCESS)
	{
		VkDebugFunc::InterpretVkResult(result);
		printf("Failed to create the command pool\n");
		return false;
	}

	return true;
}

uint32_t VkGraphics::findMemoryType(uint32_t typeFilter, VkMemoryPropertyFlags props)
{
	VkPhysicalDeviceMemoryProperties memProps;
	vkGetPhysicalDeviceMemoryProperties(mVkPhysicalDevice, &memProps);

	for (uint32_t i = 0; i < memProps.memoryTypeCount; i++)
	{
		if ((typeFilter & 1 << i) &&
			((memProps.memoryTypes[i].propertyFlags & props) == props))
		{
			return i;
		}
	}

	// Indicate failure
	return -1;
}




/****************************************************************************************/
/*                                                                                      */
/*  Private functions for creating the physical device                                  */
/*                                                                                      */
/****************************************************************************************/
void VkGraphics::getPhysicalDevice(uint32_t pDeviceCount, VkPhysicalDevice* outPhysicalDevice)
{
    std::vector<VkPhysicalDevice> physicalDevices(pDeviceCount);
    vkEnumeratePhysicalDevices(mInstance, &pDeviceCount, physicalDevices.data());

    std::priority_queue<VkPhysicalDeviceScore, std::vector<VkPhysicalDeviceScore>, DeviceScoreComparator> physicalDeviceScores;
    for (const auto& device : physicalDevices) 
	{
        int score = rateDeviceSuitability(device);
        VkPhysicalDeviceScore pds = {score, device};
        physicalDeviceScores.push(pds);
    }

    if (physicalDeviceScores.top().score > 0) 
	{
		*outPhysicalDevice = physicalDeviceScores.top().pDevice;
		findQueueFamilies(mVkPhysicalDevice, &mIndex);
    }
}

int VkGraphics::rateDeviceSuitability(VkPhysicalDevice pDevice)
{
    VkPhysicalDeviceProperties pDeviceProps;
    VkPhysicalDeviceFeatures pDeviceFeatures;
    vkGetPhysicalDeviceProperties(pDevice, &pDeviceProps);
    vkGetPhysicalDeviceFeatures(pDevice, &pDeviceFeatures);

#if _DEVICE_NAME_
    printf("Device Name: %s\n", pDeviceProps.deviceName);
#endif
       
    int score = 0;
    score = (pDeviceProps.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU) ? 1000 : 0;
    score += pDeviceProps.limits.maxImageDimension2D;

    // Geometry shader is optional, but good to have 
    if (pDeviceFeatures.geometryShader) 
	{
        score += 1000;
    }

	VkQueueFamilyIndex index = {};
	findQueueFamilies(pDevice, &index);
    if (!index.graphicsFamilyFound && !index.presentFamilyFound) 
	{
        return 0; 
    }

	if (checkDeviceExtensionSupport(pDevice) == true)
	{
		score += 1000;
	}

#if _DEVICE_SCORE_
    printf("Score: %d\n", score);
#endif

    return score;   
}

void VkGraphics::findQueueFamilies(VkPhysicalDevice& pDevice, VkQueueFamilyIndex* inIndex)
{
     // Try to get the queue families
    inIndex->graphicsFamily = 0;
    inIndex->presentFamily = 0;
    inIndex->graphicsFamilyFound = false;
    inIndex->presentFamilyFound = false;
    
    uint32_t queueFamilyCount = 0;
    vkGetPhysicalDeviceQueueFamilyProperties(pDevice, &queueFamilyCount, nullptr);
    
    std::vector<VkQueueFamilyProperties> queueFamilies(queueFamilyCount);
    vkGetPhysicalDeviceQueueFamilyProperties(pDevice, &queueFamilyCount, queueFamilies.data());

    int i = 0;
    for (const auto& queueFamily : queueFamilies) 
	{
        // Check if the queue family is supported
        if (queueFamily.queueCount > 0 && queueFamily.queueFlags & VK_QUEUE_GRAPHICS_BIT) 
		{
            inIndex->graphicsFamily = i;
            inIndex->graphicsFamilyFound = true;
        } 

        // Check for present support
        VkBool32 presentSupport;
        vkGetPhysicalDeviceSurfaceSupportKHR(pDevice, i, mVkSurface, &presentSupport);
        if (queueFamily.queueCount > 0 && presentSupport == VK_TRUE)
        {
            inIndex->presentFamily = i;
            inIndex->presentFamilyFound = true;
        }

        // We found what we need
        if (inIndex->graphicsFamilyFound && inIndex->presentFamilyFound)
        {
            break;
        }
        i++;
    }   
}

bool VkGraphics::checkDeviceExtensionSupport(VkPhysicalDevice& pDevice)
{
    uint32_t extensionCount;
	vkEnumerateDeviceExtensionProperties(pDevice, nullptr, &extensionCount, nullptr);

	std::vector<VkExtensionProperties> availableExtensions(extensionCount);
	vkEnumerateDeviceExtensionProperties(pDevice, nullptr, &extensionCount, availableExtensions.data());

	std::set<std::string> requiredExtensions(mDesiredDeviceExtensions.begin(), mDesiredDeviceExtensions.end());
	for (const auto& ext : availableExtensions)
	{
		requiredExtensions.erase(ext.extensionName);
	}
	return requiredExtensions.empty();   
}

/****************************************************************************************/
/*                                                                                      */
/*  Private functions for creating the swapchain                                        */
/*                                                                                      */
/****************************************************************************************/
// Get the surface format that is best supported with the physical devices
bool VkGraphics::querySwapChainSupport(VkPhysicalDevice& pDevice, VkSwapChainInfo* inSwapChainInfo)
{
	// Use this to check for any failures
	VkResult result;

	// Get the surface's capabilities and query it
	result = vkGetPhysicalDeviceSurfaceCapabilitiesKHR(pDevice, mVkSurface, &inSwapChainInfo->capabilities);
	if (result != VK_SUCCESS)
	{
		VkDebugFunc::InterpretVkResult(result);
		printf("Failed to get the physical device surface capabilities\n");
		return false;
	}

	// Get the supported surface format
	uint32_t surfaceFormatCount;
	result = vkGetPhysicalDeviceSurfaceFormatsKHR(pDevice, mVkSurface, &surfaceFormatCount, nullptr);
	if (result != VK_SUCCESS)
	{
		VkDebugFunc::InterpretVkResult(result);
		printf("Failed to get the supported device surface formats\n");
		return false;
	}

	// If no problem, get the supported device surfaces
	inSwapChainInfo->surfaceFormatList.resize(surfaceFormatCount);
	vkGetPhysicalDeviceSurfaceFormatsKHR(pDevice, mVkSurface, &surfaceFormatCount, inSwapChainInfo->surfaceFormatList.data());

	// Get the supported present mode
	uint32_t presentModeCount;
	result = vkGetPhysicalDeviceSurfacePresentModesKHR(pDevice, mVkSurface, &presentModeCount, nullptr);
	if (result != VK_SUCCESS)
	{
		VkDebugFunc::InterpretVkResult(result);
		printf("Failed to get the supported present mode\n");
		return false;
	}

	// If no problem, get the supported present mode
	inSwapChainInfo->presentModeList.resize(presentModeCount);
	vkGetPhysicalDeviceSurfacePresentModesKHR(pDevice, mVkSurface, &presentModeCount, inSwapChainInfo->presentModeList.data());
	return true;
}

bool VkGraphics::createImageViews()
{
	mVkSwapChainImageViews.resize(mVkSwapchainImages.size());

	for (int i = 0; i < mVkSwapChainImageViews.size(); i++)
	{
		VkImageViewCreateInfo imageViewCreateInfo = { };
		imageViewCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
		imageViewCreateInfo.image = mVkSwapchainImages[i];
		imageViewCreateInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
		imageViewCreateInfo.format = mVkSwapChainImageFormat;
		imageViewCreateInfo.components = {
			VK_COMPONENT_SWIZZLE_IDENTITY,
			VK_COMPONENT_SWIZZLE_IDENTITY,
			VK_COMPONENT_SWIZZLE_IDENTITY,
			VK_COMPONENT_SWIZZLE_IDENTITY
		};
		imageViewCreateInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		imageViewCreateInfo.subresourceRange.baseMipLevel = 0;
		imageViewCreateInfo.subresourceRange.levelCount = 1;
		imageViewCreateInfo.subresourceRange.baseArrayLayer = 0;
		imageViewCreateInfo.subresourceRange.layerCount = 1;

		VkResult res = vkCreateImageView(mVkDevice, &imageViewCreateInfo, nullptr, &mVkSwapChainImageViews[i]);
		if (res != VK_SUCCESS)
		{
			VkDebugFunc::InterpretVkResult(res);
			return false;
		}
	}
	return true;
}

bool VkGraphics::getSwapchainImageList(std::vector<VkImage>& pSwapchainImages)
{
	// Get the total number of swapchain images that were created when making the swapchain
	uint32_t imageCount = 0;
	VkResult res = vkGetSwapchainImagesKHR(mVkDevice, mVkSwapchain, &imageCount, nullptr);
	if (res != VK_SUCCESS || imageCount == 0)
	{
		VkDebugFunc::InterpretVkResult(res);
		printf("Failed to get the number of swapchain images\n");
		return false;
	}
	pSwapchainImages.resize(imageCount);
	res = vkGetSwapchainImagesKHR(mVkDevice, mVkSwapchain, &imageCount, pSwapchainImages.data());
	if (res != VK_SUCCESS)
	{
		VkDebugFunc::InterpretVkResult(res);
		printf("Failed to get the swapchain images\n");
		return false;
	}
	return true;
}

bool VkGraphics::getSwapchainImage(uint32_t* inImageIndex)
{
#if _WIN32 || _WIN64
	uint64_t maxTime = ULONG_MAX;
#elif __APPLE__ || __MACH__
	uint64_t maxTime = std::numeric_limit<uint64_t>::max()
#endif
	uint32_t tempIndex = -1;
	bool success = true;
	VkResult res = vkAcquireNextImageKHR(mVkDevice, mVkSwapchain, maxTime, 
											mVkSwapchainImageSemaphores[mCurrentFrame], 
											mVkSwapchainImageFences[mCurrentFrame], &tempIndex);
	switch (res)
	{
	case VK_SUCCESS:
		printf("VK_SUCCESS: Successfully acquired image index\n");
		success = true;
		break;
	case VK_SUBOPTIMAL_KHR:
		printf("VK_SUBOPTIMAL_KHR: Successfully acquired image index\n");
		success = true;
		break;
	case VK_ERROR_OUT_OF_DATE_KHR:
		printf("VK_ERROR_OUT_OF_DATE_KHR: Out of resources. Attempting to recreate swapchain\n");
		recreateSwapChain();
		success = true;
		break;
	default:
		success = false;
		break;
	}
	return success;
}

// Pick the best supported surface format. If not supported, go with the default
void VkGraphics::getSurfaceFormat(std::vector<VkSurfaceFormatKHR>&		supportedFormats, 
											   VkSurfaceFormatKHR&		desiredFormat, 
											   VkSurfaceFormatKHR*		outFormat)
{
	bool found = false;
	for (VkSurfaceFormatKHR& supportedFormat : supportedFormats)
	{
		if (supportedFormat.format == desiredFormat.format &&
			supportedFormat.colorSpace == desiredFormat.colorSpace)
		{
			found = true;
			*outFormat = supportedFormat;
			break;
		}
	}

	// If not found, use this format as the default
	if (!found)
	{
		*outFormat = { VK_FORMAT_B8G8R8A8_UNORM, VK_COLOR_SPACE_SRGB_NONLINEAR_KHR };
	}
}

// Pick the best supported present mode. If not supported, go with the default
void VkGraphics::getPresentMode(std::vector<VkPresentModeKHR>&		supportedPresentModes, 
											 VkPresentModeKHR&		desiredPresentMode,  
											 VkPresentModeKHR*		outPresentMode)
{
	bool found = false;
	for (VkPresentModeKHR& supportedPresentMode : supportedPresentModes)
	{
		if (supportedPresentMode == desiredPresentMode)
		{
			found = true;
			*outPresentMode = supportedPresentMode;
			break;
		}
	}

	// If not found, use this presentMode as the default
	if (!found)
	{
		*outPresentMode = VK_PRESENT_MODE_FIFO_KHR;
	}
}

void VkGraphics::getImageUsage(VkSurfaceCapabilitiesKHR&	capabilities, 
									 VkImageUsageFlags&		desiredImageUsage,
									 VkImageUsageFlags*		outImageUsage)
{
	// Bitwise AND on the supported usage and desired usage
	*outImageUsage = desiredImageUsage & capabilities.supportedUsageFlags;

	// Check to see if the output image usage is the same as the desired image usage
	if (*outImageUsage != desiredImageUsage)
	{
		*outImageUsage = 0x0;
	}
}

void VkGraphics::getSupportedImageDimension(VkSurfaceCapabilitiesKHR&	capabilities,
														  VkExtent2D*	outDimension)
{
	VkExtent2D resDimension = {};
	if (capabilities.currentExtent.width == 0xFFFFFFFF)
	{
		resDimension = { _HEIGHT_, _WIDTH_ };

		uint32_t imgMinWidth = capabilities.minImageExtent.width;
		uint32_t imgMinHeight = capabilities.minImageExtent.height;
		uint32_t imgMaxWidth = capabilities.maxImageExtent.width;
		uint32_t imgMaxHeight = capabilities.maxImageExtent.height;

		// Clamp the image width
		if (resDimension.width < imgMinWidth)
		{
			resDimension.width = imgMinWidth;
		}
		else if (resDimension.width > imgMaxWidth)
		{
			resDimension.width = imgMaxWidth;
		}

		// Clamp the image height
		if (resDimension.height < imgMinHeight)
		{
			resDimension.height = imgMinHeight;
		}
		else if (resDimension.height > imgMaxHeight)
		{
			resDimension.height = imgMaxHeight;
		}
		*outDimension = resDimension;
	}
	else
	{
		*outDimension = capabilities.currentExtent;
	}
}

void VkGraphics::getNumSwapChainImages(VkSurfaceCapabilitiesKHR&	capabilities,
													   uint32_t*	outNumImages)
{
	uint32_t tempNumImages = capabilities.minImageCount + 1;
	
	// If the maxImageCount > 0, then there is a max amount of created images.
	// If the minimum number of count is greater than the max image count, then clamp it
	// to the max image count
	if (capabilities.maxImageCount > 0 && tempNumImages > capabilities.maxImageCount)
	{
		*outNumImages = capabilities.maxImageCount;
	}
	else
	{
		*outNumImages = tempNumImages;
	}
}

void VkGraphics::getSurfaceTransformFlag(	  VkSurfaceCapabilitiesKHR&		capabilities, 
										 VkSurfaceTransformFlagBitsKHR&		desiredTransform,
										 VkSurfaceTransformFlagBitsKHR*		outTransform)
{
	// If the desired transform is supported, put it into the output transform
	// Otherwise, use the current capable transform
	if (desiredTransform & capabilities.supportedTransforms)
	{
		*outTransform = desiredTransform;
	}
	else 
	{
		*outTransform = capabilities.currentTransform;
	}
}

void VkGraphics::selectSwapChainImageFormat(std::vector<VkSurfaceFormatKHR>&	surfaceFormats,
														 VkSurfaceFormatKHR&	desiredSurfaceFormat,
																   VkFormat*	outFormat,
															VkColorSpaceKHR*	outImageColorSpace)
{
	if (surfaceFormats.size() == 1 && surfaceFormats[0].format == VK_FORMAT_UNDEFINED)
	{
		// No restriction. Pick the desired surface format we want.
		*outFormat = desiredSurfaceFormat.format;
		*outImageColorSpace = desiredSurfaceFormat.colorSpace;
		return;
	}

	// Iterate through the surface formats and find the desired match
	for (VkSurfaceFormatKHR& surfaceFormat : surfaceFormats)
	{
		if (surfaceFormat.format == desiredSurfaceFormat.format &&
			surfaceFormat.colorSpace == desiredSurfaceFormat.colorSpace)
		{
			*outFormat = desiredSurfaceFormat.format;
			*outImageColorSpace = desiredSurfaceFormat.colorSpace;
			return;
		}
	}

	// If not found, try to the find the best matched format
	for (VkSurfaceFormatKHR& surfaceFormat : surfaceFormats)
	{
		if (surfaceFormat.format == desiredSurfaceFormat.format)
		{
			printf("Could not find matching color space. Proceeded to find the matching format\n");
			*outFormat = desiredSurfaceFormat.format;
			*outImageColorSpace = desiredSurfaceFormat.colorSpace;
			return;
		}
	}

	// If still not found, use the first available format and color space
	*outFormat = surfaceFormats[0].format;
	*outImageColorSpace = surfaceFormats[0].colorSpace;
	printf("Could not find the matching color space. Proceeded to see the first available format and color space\n");
}

bool VkGraphics::createCommandBuffers()
{
	mVkCommandBuffers.resize(mVkFrameBuffer.size());

	VkCommandBufferAllocateInfo commandAllocateInfo = {};
	commandAllocateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	commandAllocateInfo.commandPool = mVkCommandPool;
	commandAllocateInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	commandAllocateInfo.commandBufferCount = (uint32_t) mVkCommandBuffers.size();

	VkResult res = vkAllocateCommandBuffers(mVkDevice, &commandAllocateInfo, mVkCommandBuffers.data());
	if (res != VK_SUCCESS)
	{
		VkDebugFunc::InterpretVkResult(res);
		printf("Failed to allocate the command buffer\n");
		return false;
	}

	// Upload the data here for now
	//mVertexBuffer.UploadData(vertices.data(), vertices.size(), sizeof(Vertex));

	// Begin the command buffer, render pass and draw commands
	for (size_t i = 0; i < mVkCommandBuffers.size(); i++)
	{
		VkCommandBufferBeginInfo vkCommandBufferBeginInfo = {};
		vkCommandBufferBeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
		vkCommandBufferBeginInfo.flags = VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT;
		vkCommandBufferBeginInfo.pInheritanceInfo = nullptr;

		res = vkBeginCommandBuffer(mVkCommandBuffers[i], &vkCommandBufferBeginInfo);
		if (res != VK_SUCCESS)
		{
			VkDebugFunc::InterpretVkResult(res);
			printf("Failed to start the command buffer\n");
			return false;
		}

		VkRenderPassBeginInfo vkRenderPassBeginInfo = {};
		vkRenderPassBeginInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
		vkRenderPassBeginInfo.renderPass = mVkRenderPass;
		vkRenderPassBeginInfo.framebuffer = mVkFrameBuffer[i];
		vkRenderPassBeginInfo.renderArea.extent = mVkSwapChainExtent;
		vkRenderPassBeginInfo.renderArea.offset = { 0, 0 };

		VkClearValue clearColor = { 0.0f, 0.0f, 0.0f, 1.0f };
		vkRenderPassBeginInfo.pClearValues = &clearColor;
		vkRenderPassBeginInfo.clearValueCount = 1;

		vkCmdBeginRenderPass(mVkCommandBuffers[i], &vkRenderPassBeginInfo, VK_SUBPASS_CONTENTS_INLINE);
		vkCmdBindPipeline(mVkCommandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, mVkPipeline);
		mVertexBuffer.SetActive(mVkCommandBuffers[i]);
		vkCmdBindDescriptorSets(mVkCommandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS,
									mVkPipelineLayout, 0, 1,
									&mVkDescSet[i], 0, nullptr);
		vkCmdDrawIndexed(mVkCommandBuffers[i], (uint32_t)indices.size(), 1, 0, 0, 0);
		vkCmdEndRenderPass(mVkCommandBuffers[i]);
		res = vkEndCommandBuffer(mVkCommandBuffers[i]);

		if (res != VK_SUCCESS)
		{
			VkDebugFunc::InterpretVkResult(res);
			printf("Failed to record the command buffer\n");
			return false;
		}
	}

	return true;
}

bool VkGraphics::createSemaphore()
{
	mVkSwapchainImageSemaphores.resize(MAX_FRAMES_IN_USE);
	mVkRenderFinishedSemaphores.resize(MAX_FRAMES_IN_USE);
	mVkSwapchainImageFences.resize(MAX_FRAMES_IN_USE);

	VkSemaphoreCreateInfo semaphoreCreateInfo = {};
	semaphoreCreateInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

	VkFenceCreateInfo fenceCreateInfo = {};
	fenceCreateInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
	fenceCreateInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;

	VkResult res = VK_SUCCESS;
	for (int i = 0; i < MAX_FRAMES_IN_USE; i++)
	{
		res = vkCreateSemaphore(mVkDevice, &semaphoreCreateInfo, nullptr, &mVkSwapchainImageSemaphores[i]);
		if (res != VK_SUCCESS)
		{
			VkDebugFunc::InterpretVkResult(res);
			printf("Failed to create swapchain image semaphore\n");
			return false;
		}

		res = vkCreateSemaphore(mVkDevice, &semaphoreCreateInfo, nullptr, &mVkRenderFinishedSemaphores[i]);
		if (res != VK_SUCCESS)
		{
			VkDebugFunc::InterpretVkResult(res);
			printf("Failed to create render finished semaphore\n");
		}

		res = vkCreateFence(mVkDevice, &fenceCreateInfo, nullptr, &mVkSwapchainImageFences[i]);
		if (res != VK_SUCCESS)
		{
			VkDebugFunc::InterpretVkResult(res);
			printf("Failed to create the swapchain fence\n");
		}
	}

	return true;
}

bool VkGraphics::CreateBuffer(VkDeviceSize			size, 
							  VkBufferUsageFlags	usage, 
							  VkMemoryPropertyFlags props,
							  VkBuffer&				buffer, 
							  VkDeviceMemory&		bufferMem)
{
	VkBufferCreateInfo vkVertBuffInfo = {};
	vkVertBuffInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
	vkVertBuffInfo.size = size;
	vkVertBuffInfo.usage = usage;
	vkVertBuffInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

	VkResult res = vkCreateBuffer(mVkDevice, &vkVertBuffInfo, nullptr, &buffer);
	if (res != VK_SUCCESS)
	{
		VkDebugFunc::InterpretVkResult(res);
		return false;
	}

	// Get the required memory type for the vertex buffer
	VkMemoryRequirements memRequirements;
	vkGetBufferMemoryRequirements(mVkDevice, buffer, &memRequirements);

	// Create the memory allocate info to get the memory type for the vertex buffer
	VkMemoryAllocateInfo allocInfo = {};
	allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
	allocInfo.allocationSize = memRequirements.size;
	allocInfo.memoryTypeIndex = findMemoryType(memRequirements.memoryTypeBits, 
											   VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);

	// Bind the device memory to the vertex buffer
	res = vkAllocateMemory(mVkDevice, &allocInfo, nullptr, &bufferMem);
	if (res != VK_SUCCESS)
	{
		VkDebugFunc::InterpretVkResult(res);
		return false;
	}
	vkBindBufferMemory(mVkDevice, buffer, bufferMem, 0);
	return true;
}

void VkGraphics::CopyBuffer(VkBuffer* dstBuff, VkBuffer* srcBuff, VkDeviceSize size)
{
	VkCommandBufferAllocateInfo allocInfo = {};
	allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	allocInfo.commandPool = mVkCommandPool;
	allocInfo.commandBufferCount = 1;

	VkCommandBuffer commandBuffer;
	VkResult res = vkAllocateCommandBuffers(mVkDevice, &allocInfo, &commandBuffer);

	// Immediately start recording command buffer
	VkCommandBufferBeginInfo commandBufferBeginInfo = {};
	commandBufferBeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	commandBufferBeginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

	// Call the copy command
	VkBufferCopy bufferCopyRegion = {};
	bufferCopyRegion.srcOffset = 0;
	bufferCopyRegion.dstOffset = 0;
	bufferCopyRegion.size = size;
	vkBeginCommandBuffer(commandBuffer, &commandBufferBeginInfo);
	vkCmdCopyBuffer(commandBuffer, *srcBuff, *dstBuff, 1, &bufferCopyRegion);
	vkEndCommandBuffer(commandBuffer);

	// Now finish the buffer transfer
	VkSubmitInfo submitInfo = {};
	submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
	submitInfo.commandBufferCount = 1;
	submitInfo.pCommandBuffers = &commandBuffer;
	vkQueueSubmit(mPresentQueue, 1, &submitInfo, VK_NULL_HANDLE);
	vkQueueWaitIdle(mPresentQueue);

	vkFreeCommandBuffers(mVkDevice, mVkCommandPool, 1, &commandBuffer);
}

void VkGraphics::drawFrame()
{
	uint32_t imageIndex = 0;
#if _WIN32 || _WIN64
	vkWaitForFences(mVkDevice, 1, &mVkSwapchainImageFences[mCurrentFrame], VK_TRUE, ULONG_MAX);
	VkResult res = vkAcquireNextImageKHR(mVkDevice, mVkSwapchain, 
						  ULONG_MAX, mVkSwapchainImageSemaphores[mCurrentFrame], 
						  VK_NULL_HANDLE, &imageIndex);
#elif __APPLE__ && __MACH__
	vkWaitForFences(mVkDevice, 1, &mVkSwapchainImageFences[mCurrentFrame], VK_TRUE, std::numeric_limits<uint64_t>::max());
	VkResult res = vkAcquireNextImageKHR(mVkDevice, mVkSwapchain, 
						  std::numeric_limits<uint64_t>::max(), 
						  mVkSwapchainImageSemaphore, VK_NULL_HANDLE, 
						  &imageIndex);
#endif

	switch (res) {
	case VK_SUCCESS:
	case VK_SUBOPTIMAL_KHR:
		break;
	case VK_ERROR_OUT_OF_DATE_KHR:
		recreateSwapChain();
		break;
	default:
		if (mFrameBufferResized)
		{
			mFrameBufferResized = false;
			recreateSwapChain();
			break;
		}
		return;
	}

	updateUniformBuffer(imageIndex);

	VkSubmitInfo vkSubmitInfo = {};
	vkSubmitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
	
	VkSemaphore waitSemaphores[] = { mVkSwapchainImageSemaphores[mCurrentFrame] };
	VkPipelineStageFlags waitStages[] = { VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT };
	vkSubmitInfo.waitSemaphoreCount = 1;
	vkSubmitInfo.pWaitSemaphores = waitSemaphores;
	vkSubmitInfo.pWaitDstStageMask = waitStages;
	vkSubmitInfo.commandBufferCount = 1;
	vkSubmitInfo.pCommandBuffers = &mVkCommandBuffers[imageIndex];

	VkSemaphore signalSemaphores[] = { mVkRenderFinishedSemaphores[mCurrentFrame] };
	vkSubmitInfo.signalSemaphoreCount = 1;
	vkSubmitInfo.pSignalSemaphores = signalSemaphores;

	vkResetFences(mVkDevice, 1, &mVkSwapchainImageFences[mCurrentFrame]);  // Have to manually reset VkFence

	res = vkQueueSubmit(mPresentQueue, 1, &vkSubmitInfo, mVkSwapchainImageFences[mCurrentFrame]);
	if (res != VK_SUCCESS)
	{
		VkDebugFunc::InterpretVkResult(res);
		printf("Failed to submit to queue\n");
		return;
	}

	VkPresentInfoKHR vkPresentInfo = {};
	vkPresentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
	vkPresentInfo.waitSemaphoreCount = 1;
	vkPresentInfo.pWaitSemaphores = signalSemaphores;
	vkPresentInfo.swapchainCount = 1;
	
	VkSwapchainKHR swapChains[] = { mVkSwapchain };
	vkPresentInfo.swapchainCount = 1;
	vkPresentInfo.pSwapchains = swapChains;
	vkPresentInfo.pImageIndices = &imageIndex;
	vkPresentInfo.pResults = nullptr;
	res = vkQueuePresentKHR(mPresentQueue, &vkPresentInfo);

	switch (res) {
	case VK_SUCCESS:
	case VK_SUBOPTIMAL_KHR:
		break;
	case VK_ERROR_OUT_OF_DATE_KHR:
		recreateSwapChain();
		break;
	default:
		if (mFrameBufferResized)
		{
			mFrameBufferResized = false;
			recreateSwapChain();
			break;
		}
		return;
	}

	mCurrentFrame = (mCurrentFrame + 1) % MAX_FRAMES_IN_USE;
}

void VkGraphics::updateUniformBuffer(uint32_t currImage)
{
	static auto startTime = std::chrono::high_resolution_clock::now();

	auto currTime = std::chrono::high_resolution_clock::now();
	float time = std::chrono::duration<float, std::chrono::seconds::period>(currTime - startTime).count();

	// Create the projection, view, and modelToWorld matrices
	VkMatrixBuff matBuff = {};
	float rotSpeed = time * Math::ToRadians(90.0f);
	
	matBuff.modelToWorld = Matrix4::CreateRotationZ(rotSpeed);
	matBuff.view = Matrix4::CreateLookAt(Vector3(2.0f, 2.0f, 2.0f), Vector3(0.0f, 0.0f, 0.0f), Vector3(0.0f, 0.0f, -1.0f));
	matBuff.view.Invert();
	matBuff.proj = Matrix4::CreatePerspectiveFOV(Math::ToRadians(45.0f), (float) mVkSwapChainExtent.width, (float) mVkSwapChainExtent.height, 0.1f, 10.0f);

	// Map the data to the uniform location
	void* data = nullptr;
	vkMapMemory(mVkDevice, mVkUniformBufferMemory[currImage], 0, sizeof(matBuff), 0, &data);
	memcpy(data, &matBuff, sizeof(matBuff));
	vkUnmapMemory(mVkDevice, mVkUniformBufferMemory[currImage]);
}
