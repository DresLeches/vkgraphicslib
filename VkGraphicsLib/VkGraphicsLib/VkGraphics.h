#pragma once
#include "stdafx.h"
#include "VkDebug.h"
#include "VkVertexBuffer.h"

struct VkPhysicalDeviceScore 
{
    int score;
    VkPhysicalDevice pDevice;
};

struct VkQueueFamilyIndex 
{
    VkQueueFamilyIndex()
        : graphicsFamily(-1), graphicsFamilyFound(false),
		  presentFamily(-1), presentFamilyFound(false)
    {
	}

    uint32_t graphicsFamily;
    uint32_t presentFamily;
    bool graphicsFamilyFound;
    bool presentFamilyFound;
};

struct VkSwapChainInfo 
{
	// Selected information for swapchain
	uint32_t numImages;
	VkSurfaceCapabilitiesKHR capabilities;
	VkExtent2D imageSize;
	VkSurfaceFormatKHR surfaceFormat;
	VkPresentModeKHR presentMode;
	VkImageUsageFlags imageUsage;
	VkSurfaceTransformFlagBitsKHR transformFlag;
	VkFormat imageFormat;
	VkColorSpaceKHR imageColorSpace;

	// Supported formats and present modes
	std::vector<VkSurfaceFormatKHR> surfaceFormatList;
	std::vector<VkPresentModeKHR> presentModeList;
};

struct SwapchainPresentInfo 
{
	VkSwapchainKHR swapChain;
	uint32_t imageIndex;
};

struct VkMatrixBuff
{
	Matrix4 modelToWorld;
	Matrix4 proj;
	Matrix4 view;
};

class VkVertexBuffer;


class VkGraphics {
public:
    VkGraphics();
    ~VkGraphics();

    void InitVulkan();
    void Run();
    void CleanUp();
	bool CreateVertexBuffer(VkBuffer& vertBuff, VkDeviceMemory& deviceMem);
	bool CreateIndexBuffer(VkBuffer& indBuff, VkDeviceMemory& deviceMem);
	bool CreateBuffer(VkDeviceSize			size,
					  VkBufferUsageFlags	usage,
					  VkMemoryPropertyFlags props,
					  VkBuffer&				buffer,
					  VkDeviceMemory&		bufferMem);
	void CopyBuffer(VkBuffer* srcBuff, VkBuffer* dstBuff, VkDeviceSize size);

	uint32_t findMemoryType(uint32_t typeFilter, VkMemoryPropertyFlags props);

	// Getter for the device
	VkDevice* GetDevice() { return &mVkDevice; }

	// Getter and setter functions
	void ToggleBufferFrameResized(bool setOn) { mFrameBufferResized = setOn; }

private:
	
    GLFWwindow* mWindow;
	VkInstance mInstance;
	VkDebugUtilsMessengerEXT callback;
    VkPhysicalDevice mVkPhysicalDevice;
    VkDevice mVkDevice;
    VkQueueFamilyIndex mIndex;
    VkQueue mPresentQueue;
    VkDebugWindow mVkDebugWindow;
	VkSurfaceKHR mVkSurface;
	VkShaderModule mVkVertexShaderModule;
	VkShaderModule mVkFragShaderModule;
	VkPipelineLayout mVkPipelineLayout;
	VkRenderPass mVkRenderPass;
	VkDescriptorSetLayout mVkDescSetLayout;
	VkDescriptorPool mVkDescPool;
	VkPipeline mVkPipeline;
	VkCommandPool mVkCommandPool;
	VkSwapchainKHR mVkSwapchain;
	VkSwapchainKHR mOldVkSwapchain;
	std::vector<VkSemaphore> mVkSwapchainImageSemaphores;
	std::vector<VkSemaphore> mVkRenderFinishedSemaphores;
	std::vector<VkFence> mVkSwapchainImageFences;
	VkFormat mVkSwapChainImageFormat;
	VkExtent2D mVkSwapChainExtent;
	size_t mCurrentFrame;
	bool mFrameBufferResized;
	VkVertexBuffer mVertexBuffer;

	std::vector<VkExtensionProperties> mVkExtensions;
	std::vector<VkFramebuffer> mVkFrameBuffer;
	std::vector<VkCommandBuffer> mVkCommandBuffers;
	std::vector<VkBuffer> mVkUniformBuffer;
	std::vector<VkDeviceMemory> mVkUniformBufferMemory;
    std::vector<VkImage> mVkSwapchainImages;
	std::vector<VkImageView> mVkSwapChainImageViews;
	std::vector<const char*> mDesiredExtensions;
	std::vector<const char*> mDesiredDeviceExtensions;
    std::vector<const char*> mSupportedValidationLayers;
	std::vector<VkDescriptorSet> mVkDescSet;

    // Functions for initializing the vulkan library
    void createInstance();
    bool checkExtensionSupport(std::vector<VkExtensionProperties> &extensions,
                               const char** glfwExtensions, uint32_t extCount);
    bool checkValidationLayerSupport();
	void enumerateVkExtensions();
    void getRequiredGLFWExtensions(std::vector<const char*>& ext);
    void setUpDebugCallback();
	bool createVkSurface();
    bool createPhysicalDevice();
    bool initLogicalDevice();
	void initBuffers();
	void createUniformBuffers();
	bool createDescriptorPool();
	bool createDescriptorSets();
    bool initSwapChain();
	void recreateSwapChain();
	VkExtent2D chooseSwapExtent(const VkSurfaceCapabilitiesKHR& capabilities);
	void cleanupSwapChain();
	bool createRenderPass();
	bool createDescriptorSetLayout();
    bool createGraphicsPipeline();
	bool readShaderFile(const std::string& fileName, std::vector<char>& outBuffer);
	bool createShaderModule(const std::vector<char>& shader, VkShaderModule* outShaderModule);
	bool createFrameBuffers();
	bool createCommandPool();
	bool createCommandBuffers();
	bool createSemaphore();

	void drawFrame();
	void updateUniformBuffer(uint32_t currImage);
    
    // Functions for initializing the Vulkan devices
    void getPhysicalDevice(uint32_t pDeviceCount, VkPhysicalDevice* outPhysicalDevice);
    int rateDeviceSuitability(VkPhysicalDevice pDevice);
    void findQueueFamilies(VkPhysicalDevice& pDevice, VkQueueFamilyIndex* inIndex);
	bool checkDeviceExtensionSupport(VkPhysicalDevice& pDevice);

    // Functions for initializing swapchain
	bool querySwapChainSupport(VkPhysicalDevice& pDevice, VkSwapChainInfo* inSwapChainInfo);
	bool getSwapchainImageList(std::vector<VkImage>& pSwapchainImages);
	bool getSwapchainImage(uint32_t* inImageIndex);
	bool createImageViews();
	void getSurfaceFormat(std::vector<VkSurfaceFormatKHR>&	supportedFormats,
						  VkSurfaceFormatKHR&				desiredFormat,
						  VkSurfaceFormatKHR*				outFormat);
	void getPresentMode(std::vector<VkPresentModeKHR>&	supportedPresentModes,
						VkPresentModeKHR&				desiredPresentMode,
						VkPresentModeKHR*				outPresentMode);
	void getSupportedImageDimension(VkSurfaceCapabilitiesKHR& capabilities,
									VkExtent2D*				  outDimension);
	void getNumSwapChainImages(VkSurfaceCapabilitiesKHR& capabilities,
							   uint32_t*				 outNumImages);
	void getImageUsage(VkSurfaceCapabilitiesKHR&	capabilities, 
					   VkImageUsageFlags&			desiredImageUsage,
					   VkImageUsageFlags*			outImageUsage);
	void getSurfaceTransformFlag(VkSurfaceCapabilitiesKHR&			capabilities, 
								 VkSurfaceTransformFlagBitsKHR&		desiredTransform,
								 VkSurfaceTransformFlagBitsKHR*		outTransform);
	void selectSwapChainImageFormat(std::vector<VkSurfaceFormatKHR>& surfaceFormats,
									VkSurfaceFormatKHR&				 desiredSurfaceFormat,
									VkFormat*						 outFormat,
								    VkColorSpaceKHR*				 outImageColorSpace);
};

