#include "VkVertexBuffer.h"
#include "VkGraphics.h"

VkVertexBuffer::VkVertexBuffer()
{
}

VkVertexBuffer::~VkVertexBuffer()
{
}

void VkVertexBuffer::Initialize(VkGraphics* graphics)
{
	mGraphics = graphics;
	mDevice = mGraphics->GetDevice();
	mGraphics->CreateVertexBuffer(mVertexBuffer, mVertexBufferMemory);
	mGraphics->CreateIndexBuffer(mIndexBuffer, mIndexBufferMemory);
}

void VkVertexBuffer::Release()
{
	vkDestroyBuffer(*mDevice, mVertexBuffer, nullptr);
	vkFreeMemory(*mDevice, mVertexBufferMemory, nullptr);
	vkDestroyBuffer(*mDevice, mIndexBuffer, nullptr);
	vkFreeMemory(*mDevice, mIndexBufferMemory, nullptr);
}

void VkVertexBuffer::UploadData(void* vertData, int numVertices, size_t vertStride)
{
	// Create the stage buffer
	VkBuffer stagingBuffer;
	VkDeviceMemory stagingMemory;
	VkDeviceSize bufferSize = numVertices * vertStride;
	mGraphics->CreateBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, 
								VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, stagingBuffer, stagingMemory);

	// Map the buffer memory
	void* data = nullptr;
	vkMapMemory(*mDevice, stagingMemory, 0, bufferSize, 0, &data);
	memcpy(data, vertData, bufferSize);
	vkUnmapMemory(*mDevice, stagingMemory);

	// Copy the buffer
	mGraphics->CopyBuffer(&mVertexBuffer, &stagingBuffer, bufferSize);
	vkDestroyBuffer(*mDevice, stagingBuffer, nullptr);
	vkFreeMemory(*mDevice, stagingMemory, nullptr);
}

void VkVertexBuffer::SetActive(VkCommandBuffer& commandBuff)
{
	VkBuffer vertBuffs[] = {
		mVertexBuffer
	};
	VkDeviceSize offsets[] = {
		0
	};

	vkCmdBindVertexBuffers(commandBuff, 0, 1, vertBuffs, offsets);
	vkCmdBindIndexBuffer(commandBuff, mIndexBuffer, 0, VK_INDEX_TYPE_UINT16);
}