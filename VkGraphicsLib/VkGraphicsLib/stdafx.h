#pragma once

// Frequently used libraries
#if _WIN32 || _WIN64
#include <Windows.h>
#endif
#include <vulkan/vulkan.h>
#include <GLFW/glfw3.h>
#include <cstdlib>
#include <cstdio>
#include <vector>
#include <cstdint>
#include <cstring>
#include <string>
#include <fstream>
#include <queue>
#include <set>
#include <chrono>
#include <xmmintrin.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#if __APPLE__ && __MACH__
#include <limits>
#endif
#include "VkDebug.h"
#include "engineMath.h"

// Frequently used preprocessor
#define _WIDTH_ 1280
#define _HEIGHT_ 1100
#define _DEVICE_NAME_ 0
#define _DEVICE_SCORE_ 0

#if __APPLE__ && __MACH__
static bool _MOLTEN_VK_ = true;
#elif _WIN32 || _WIN64
static bool _MOLTEN_VK_ = false;
#endif
